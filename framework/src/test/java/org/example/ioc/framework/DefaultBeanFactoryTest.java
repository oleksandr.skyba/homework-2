package org.example.ioc.framework;

import org.example.ioc.framework.definition.BasicBeanDefinition;
import org.example.ioc.framework.definition.BeanDefinitionRegistry;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DefaultBeanFactoryTest {

    @Mock
    static BeanDefinitionRegistry beanDefinitionRegistry;

    @BeforeAll
    static void init() {

        beanDefinitionRegistry = mock(BeanDefinitionRegistry.class);
        when(beanDefinitionRegistry
                .getBeanDefinition("bean"))
                .thenReturn(new BasicBeanDefinition("bean", Stub.class, null));
    }

    @Test
    void shouldReturnSameBean() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 100; i++) {

            BeanFactory beanFactory = new DefaultBeanFactory(beanDefinitionRegistry);
            Queue<Stub> values = new ConcurrentLinkedQueue<Stub>();
            CountDownLatch latch = new CountDownLatch(6);

            for (int j = 0; j < 6; j++) {
                executorService.submit(()->{

                    var bean = beanFactory.getBean("bean");
                    values.add((Stub)bean);
                    latch.countDown();
                });
            }

            latch.await();

            assertThat(values.stream().map(e -> e.getValue()).distinct().collect(Collectors.toList()).size()).isEqualTo(1);
        }
    }
}