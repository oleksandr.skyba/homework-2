package org.example.ioc.framework;

import lombok.Getter;

import java.util.concurrent.atomic.AtomicInteger;

public class Stub {
    private static AtomicInteger counter = new AtomicInteger();
    @Getter
    private final int value;

    public Stub() {
        value = counter.incrementAndGet();
    }
}
